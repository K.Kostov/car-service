//return select user ID and list him cars
$('#selectCustomer').on('change', (function () {
    const userId = $('#selectCustomer').val();
    $.ajax({
        type: 'GET',
        url: '/customer-cars/' + userId,
        dataType: 'json',
        success: function (data) {
            $.each(data, function (i) {
                const option = "<option value = " + data[i].id + ">" + data[i].licensePlate + "</option>";
                $("#selectCar").append(option);
            });
        },
        error: function () {
            alert('error');
        }
    })
}));
let lastVisitId;
//save selected customer car to table 'visit' in DB
$('#selectCar').on('change', function () {
    const customerCarId = $('#selectCar').val();
    if (confirm('Do you want to get the car into the workshop')) {
        $.ajax({
            type: 'POST',
            url: '/add-visit',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({customerCar: {id: customerCarId}, totalPrice: 0}),
            dataType: 'json',
            success: function (data) {
                lastVisitId = data;
            }
        });
    }
});
let totalPrice = 0;
// add services to 'Selected Services' and 'visit_services' in DB
$('.service-button').on('click', event, (function () {
    let btnTarget = event.target;
    let btnText = $(btnTarget).text();
    const serviceId = $(btnTarget).val();
    $('.list').append("<b><li class='addedService' id=" + serviceId + ">" + btnText + "</li></b>");
    btnTarget.disabled = true;
    $(btnTarget).addClass('selected-service');
    $.ajax({
        type: 'POST',
        url: '/' + lastVisitId + '/add-service/' + serviceId,
        dataType: 'json',
        success: function (data) {
            totalPrice = data;
            $('#totalPrice').replaceWith("<span id='totalPrice'>Total Price: " + totalPrice + "</span>");
        }
    });
}));
// remove service form 'Selected Services' and 'visit_services' in DB
$('.list').on('click', 'li', event, (function () {
    let targetLi = $(event.target);
    let serviceId = $(event.target).attr('id');
    targetLi.remove();
    $('#' + serviceId).prop('disabled', false).removeClass('selected-service');
    $.ajax({
        type: 'DELETE',
        url: '/' + lastVisitId + '/delete-service/' + serviceId,
        dataType: 'json',
        success: function (data) {
            totalPrice = data;
            $('#totalPrice').replaceWith("<span id='totalPrice'>Total Price: " + totalPrice + "</span>");
        }
    });
}));
//finish the order and set Total Price
$('#finished-the-order').on('click', function () {
    if (confirm('Аre you sure you want to complete the order')) {
        $.ajax({
            type: 'PUT',
            url: '/' + lastVisitId + '/set-price/' + totalPrice,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
        })
    }
});
//download pdf with order details
$('#download-pdf').on('click', function () {
    if (confirm('Аre you sure you want to download PDF')) {
        $.ajax({
            type: 'GET',
            url: '/getVisitByIdAndGeneratePdf/' + lastVisitId,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            success:
                window.location.href = '/admin'
        })
    }
});
//delete service with services manager
$('.del-button').on('click', event, function () {
    let serviceId = $(event.target).attr('id');
    if (confirm('Are you sure you want to delete this service?')) {
        $('#row' + serviceId).remove();
        $.ajax({
            type: 'DELETE',
            url: '/delete-services/' + serviceId,
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
        });
    }
});
//open add new service blank
$('#add-new-service').on('click', function () {
    $('.add-service-blank').show(800);
});
//cancel adding new service
$('#cancel-btn').on('click', function () {
    $('.add-service-blank').hide(800);
});
//add new service
$('#save-btn').on('click', function () {
    let nameField = $('#service-name').val();
    let priceField = $('#service-price').val();
    let newServiceId = 0;
    $.ajax({
        type: 'POST',
        url: '/add-new-service',
        contentType: 'application/json; charset=utf-8',
        data: JSON.stringify({name: nameField, price: priceField}),
        dataType: 'json',
        success: function (data) {
            newServiceId = data;
        }
    });
    $('#table-body').append(
        "<tr class='row' id=" + 'row' + newServiceId + ">" +
        "<td id=" + 'name' + newServiceId + " class='td-data'>" + nameField + "</td>" +
        "<td id=" + 'price' + newServiceId + " class='td-data'>" + priceField + "</td>" +
        "<td class='del-wrapper td-data'>" +
        "<input type='submit' value='Edit' class='edit-button' id=" + newServiceId + "/>" +
        "<input type='submit' value='Save' style='display: none' class='save-btn' id=" + 'save' + newServiceId + "/>" +
        "</td>" +
        "<td class='del-wrapper td-data'>" +
        "<input type='submit' value='Delete' id=" + newServiceId + " class='del-button'/>" +
        "</td>" +
        "</tr>"
    );
    $('.add-service-blank').hide(800);
});
//edit functionality
$('.edit-button').on('click', event, function () {
    let servId = $(event.target).attr('id');
    let servName = $('#name' + servId).html();
    let servPrice = $('#price' + servId).html();
    $(this).hide();
    $('#save' + servId).show();
    $('#name' + servId).replaceWith("\"<td  class='td-data' id=" + 'name' + servId + "><input type='text' id=" + 'input-name' + servId + " class = 'input-name' minlength='3'></td>");
    $('#price' + servId).replaceWith("<td  class='td-data' id=" + 'price' + servId + "><input id=" + 'input-price' + servId + " type='number' min = '0' class = 'input-price'></td>");
    $('#input-name' + servId).val(servName);
    $('#input-price' + servId).val(servPrice);
});
//save edited service
$('.save-btn').on('click', event, function () {
    let saveServId = $(event.target).attr('id');
    let servId = saveServId.substr(4);
    let newName = $('#' + 'input-name' + servId).val();
    let newPrice = $('#' + 'input-price' + servId).val();
    if ($.isNumeric(newName)) {
        alert("The Service Name can not be digit!");
    } else if (newPrice < 0) {
        alert("Price can not be negative");
    } else {
        let newPrice = $('#' + 'input-price' + servId).val();
        $.ajax({
            type: 'PUT',
            url: '/edit-service',
            contentType: 'application/json; charset=utf-8',
            data: JSON.stringify({id: servId, name: newName, price: newPrice}),
            dataType: 'json',
        });
        $('#name' + servId).replaceWith("<td id=" + 'name' + servId + " class='td-data'>" + newName + "</td>");
        $('#price' + servId).replaceWith("<td id=" + 'price' + servId + " class='td-data'>" + newPrice + "</td>");
        $('#' + saveServId).hide();
        $('#' + servId).show();
    }
});

























