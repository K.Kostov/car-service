package com.fp.carservice.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
@Table(name = "customers")
public class Customer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    @NotNull(message = "Name is required")
    @Size(min = 2, message = "Name must be at least 2 characters.")
    private String name;

    @Column(name = "phone")
    @NotNull(message = "Phone is required")
    @Size(min = 5, message = "Phone must be at least 5 symbols.")
    private String phone;

    @Column(name = "email")
    @NotNull(message = "Email is required")
    @Size(min = 4, message = "Email must be at least 4 symbols.")
    private String email;

    @Column(name = "isDeleted")
    private boolean isDeleted;

    public Customer() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isDeleted() {
        return isDeleted;
    }

    public void setDeleted(boolean deleted) {
        isDeleted = deleted;
    }
}
