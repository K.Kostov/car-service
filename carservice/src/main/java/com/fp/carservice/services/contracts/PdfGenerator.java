package com.fp.carservice.services.contracts;

import com.itextpdf.text.DocumentException;
import com.fp.carservice.models.Visit;

import java.io.IOException;

public interface PdfGenerator {

    void createPdf (Visit visit) throws DocumentException, IOException;
}
