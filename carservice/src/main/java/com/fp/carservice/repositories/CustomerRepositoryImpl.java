package com.fp.carservice.repositories;

import com.fp.carservice.models.Customer;
import com.fp.carservice.models.Services;
import com.fp.carservice.repositories.contracts.CustomerRepository;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CustomerRepositoryImpl implements CustomerRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public CustomerRepositoryImpl (SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Customer getCustomerById(int id) {
        Customer customer = null;
        try (Session session = sessionFactory.openSession()) {
            customer = session.get(Customer.class, id);
        }
        if (customer == null || customer.isDeleted()) {
            throw new RuntimeException(String.format("Customer with id %d not found.", id));
        }
        return customer;
    }

    @Override
    public List<Customer> listAllCustomers() {
        String hql = "FROM Customer c WHERE c.isDeleted < 1";
        try (Session session = sessionFactory.openSession()) {
            Query<Customer> query = session.createQuery(hql, Customer.class);
            return query.list();
        }
    }

    @Override
    public void addCustomer(Customer customer) {
        try (Session session = sessionFactory.openSession()) {
            session.save(customer);
        }
    }

    @Override
    public void editCustomer(Customer newCustomer) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Customer customer = session.get(Customer.class, newCustomer.getId());

            customer.setName(newCustomer.getName());
            customer.setEmail(newCustomer.getEmail());
            customer.setPhone(newCustomer.getPhone());

            session.getTransaction().commit();
        }
    }

    @Override
    public void deleteCustomerByID(int id) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            Customer customer = session.get(Customer.class, id);

            customer.setDeleted(true);

            session.getTransaction().commit();
        }
    }

    @Override
    public Customer getCustomerByEmail(String customerEmail) {
        String hql = "FROM Customer c WHERE c.email = :email AND c.isDeleted < 1";
        try (Session session = sessionFactory.openSession()) {
            Query<Customer> query = session.createQuery(hql, Customer.class);
            query.setParameter("email", customerEmail);
            return query.list().get(0);
        }
    }
}
