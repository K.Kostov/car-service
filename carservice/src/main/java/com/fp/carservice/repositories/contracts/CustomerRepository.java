package com.fp.carservice.repositories.contracts;

import com.fp.carservice.models.Customer;

import java.util.List;

public interface CustomerRepository {

    Customer getCustomerById(int id);

    List<Customer> listAllCustomers();

    void addCustomer(Customer customer);

    void editCustomer(Customer customer);

    void deleteCustomerByID(int id);

    Customer getCustomerByEmail(String customerEmail);
}
