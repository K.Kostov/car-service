package com.fp.carservice.repositories;

import com.fp.carservice.models.CustomerCar;
import com.fp.carservice.repositories.contracts.CustomerCarRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CustomerCarRepositoryImpl implements CustomerCarRepository {
    private SessionFactory sessionFactory;

    @Autowired
    public CustomerCarRepositoryImpl(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public CustomerCar getCustomerCarById(int id) {
        CustomerCar customerCar = null;
        try (Session session = sessionFactory.openSession()) {
            customerCar = session.get(CustomerCar.class, id);
        }
        if (customerCar == null || customerCar.isDeleted()) {
            throw new RuntimeException(String.format("Customer Car with id %d not found.", id));
        }
        return customerCar;
    }

    @Override
    public List<CustomerCar> getAllCustomerCars(int customerId) {
        String hql = "FROM CustomerCar cc WHERE cc.customer.id = :customerId AND cc.isDeleted < 1";
        try (Session session = sessionFactory.openSession()) {
            Query<CustomerCar> query = session.createQuery(hql, CustomerCar.class);
            query.setParameter("customerId", customerId);
            return query.list();
        }
    }

    @Override
    public List<CustomerCar> listAllCustomerCars() {
        String hql = "FROM CustomerCar cc WHERE cc.isDeleted < 1";
        try (Session session = sessionFactory.openSession()) {
            Query<CustomerCar> query = session.createQuery(hql, CustomerCar.class);
            return query.list();
        }
    }

    @Override
    public void addCustomerCar(CustomerCar customerCar) {
        try (Session session = sessionFactory.openSession()) {
            session.save(customerCar);
        }
    }

    @Override
    public void editCustomerCar(CustomerCar newCustomerCar) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            CustomerCar customerCar = session.get(CustomerCar.class, newCustomerCar.getId());

            customerCar.setCar(newCustomerCar.getCar());
            customerCar.setCustomer(newCustomerCar.getCustomer());
            customerCar.setLicensePlate(newCustomerCar.getLicensePlate());
            customerCar.setVIN(newCustomerCar.getVIN());

            session.getTransaction().commit();
        }
    }

    @Override
    public void deleteCustomerCarByID(int id) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            CustomerCar customerCar = session.get(CustomerCar.class, id);

            customerCar.setDeleted(true);

            session.getTransaction().commit();
        }
    }
}
