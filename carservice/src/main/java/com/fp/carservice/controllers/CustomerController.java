package com.fp.carservice.controllers;

import com.fp.carservice.models.Customer;
import com.fp.carservice.models.User;
import com.fp.carservice.services.contracts.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.provisioning.UserDetailsManager;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.security.Principal;

@Controller
@RequestMapping
public class CustomerController {
    private CustomerService customerService;
    private UserDetailsManager userDetailsManager;

    @Autowired
    public CustomerController(CustomerService customerService, UserDetailsManager userDetailsManager) {
        this.customerService = customerService;
        this.userDetailsManager = userDetailsManager;
    }

    @GetMapping("customers/edit")
    public String getCustomerEditForm(Model model, Principal principal) {

        User user = new User();
        Customer currentCustomer = customerService.getCustomerByEmail(principal.getName());
        user.setName(currentCustomer.getName());
        user.setPhone(currentCustomer.getPhone());
        user.setUsername(currentCustomer.getEmail());

        model.addAttribute("user", user);
        return "edit-user";
    }

    @PostMapping("customers/edit")
    public String editUser(RedirectAttributes redirectAttributes,
                           @ModelAttribute User user) {

        customerService.editCustomer(user);

        return "redirect:/";
    }

    @GetMapping("customers/forgot-password")
    public String getForgottenPassword(Model model) {

        model.addAttribute("user", new User());
        return "forgot-password";
    }

    @PostMapping("customers/forgot-password")
    public String changeForgottenPassword(RedirectAttributes redirectAttributes, @ModelAttribute User user, Model model) {

        if (!userDetailsManager.userExists(user.getUsername())) {
            model.addAttribute("user", new User());
            model.addAttribute("exists", "User does not exist!");
            return "forgot-password";
        }
        customerService.passwordReset(user);
        return "redirect:/";
    }

    @GetMapping("admin/customers/edit/{id}")
    public String getCustomerEditByAdminForm(Model model, @PathVariable int id) {

        User user = new User();
        Customer currentCustomer = customerService.getCustomerById(id);
        user.setName(currentCustomer.getName());
        user.setPhone(currentCustomer.getPhone());
        user.setUsername(currentCustomer.getEmail());

        model.addAttribute("user", user);
        return "edit-user";
    }

    @GetMapping("admin/customers/delete/{username}")
    public String deleteCustomer(@PathVariable String username) {
        customerService.deleteCustomer(username);
        return "redirect:/";
    }
}
