package com.fp.carservice.controllers.restControllers;

import com.fp.carservice.models.CustomerCar;
import com.fp.carservice.services.contracts.CustomerCarService;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/customer-cars")
public class CustomerCarRestController {

    private CustomerCarService customerCarService;

    @Autowired
    public CustomerCarRestController(CustomerCarService customerCarService) {
        this.customerCarService = customerCarService;
    }

    @GetMapping
    public List<CustomerCar> listAllCustomerCars() {
        return customerCarService.listAllCustomerCars();
    }

    @GetMapping("/all-by-customer/{id}")
    public List<CustomerCar> getAllCustomerCars(@PathVariable int id) {
        return customerCarService.getAllCustomerCars(id);
    }

    @GetMapping("/{id}")
    public CustomerCar getCustomerCarById(@PathVariable int id) {
        return customerCarService.getCustomerCarById(id);
    }

    @PostMapping
    public void addBeer(@Valid @RequestBody CustomerCar customerCar) {
        customerCarService.addCustomerCar(customerCar);
    }

    @PutMapping
    public void editCustomerCar(@Valid @RequestBody CustomerCar customerCar) {
        customerCarService.editCustomerCar(customerCar);
    }

    @DeleteMapping("/{id}")
    public void deleteCustomerCar(@PathVariable int id) {
        customerCarService.deleteCustomerCarByID(id);
    }
}
