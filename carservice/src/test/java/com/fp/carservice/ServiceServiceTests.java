package com.fp.carservice;

import com.fp.carservice.models.Services;
import com.fp.carservice.repositories.contracts.ServiceRepository;
import com.fp.carservice.services.ServiceServiceImpl;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ServiceServiceTests {

    @Mock
    ServiceRepository mockServiceRepository;

    @InjectMocks
    ServiceServiceImpl service;

    private Services MOT = new Services("MOT", 45.00);
    private Services acService = new Services("AC Service", 50.00);
    private List<Services> services = new ArrayList<>();

    @Before
    public void setDefaultTestServices() {
        MOT.setId(1);
        acService.setId(2);
        services.add(MOT);
        services.add(acService);
    }

    @Test
    public void getAllServices_ShouldReturnFourServices() {
        //Arrange
        when(mockServiceRepository.listAllServices()).thenReturn(services);
        //Act
        List<Services> result = service.listAllServices();
        //Assert
        Assert.assertEquals(2, result.size());
        Assert.assertEquals("MOT", result.get(0).getName());
        Assert.assertEquals("AC Service", result.get(1).getName());
    }

    @Test
    public void getServiceById_ShouldReturnServiceWithIdTwo_WhenGiveIdIsTwo() {
        //Arrange
        when(mockServiceRepository.getServiceById(2)).thenReturn(acService);
        //Act
        Services result = service.getServiceById(2);
        //Assert
        Assert.assertEquals(2, result.getId());
        Assert.assertEquals("AC Service", result.getName());
    }

    @Test(expected = RuntimeException.class)
    public void getServiceById_ShouldReturnHibernateException_WhenGiveIdNotExist() {
        doThrow().when(mockServiceRepository).getServiceById(isA(Integer.class));
        service.getServiceById(5);
    }

    @Test
   public void addService_ShouldReturnIdOfNewAddedService() {
        //Act
        Services checkLight = new Services();
        when(mockServiceRepository.addService(checkLight)).thenReturn(3);
        int result = service.addService(checkLight);
        //Assert
        Assert.assertEquals(3, result);
    }

    @Test
    public void whenDeleteService_ShouldSetIsDeleteToTrue() {
        Services newServices = mock(Services.class);
        newServices.setId(3);
        services.add(newServices);
        service.deleteService(3);
        verify(mockServiceRepository, times(1)).deleteService(3);
    }

    @Test
    public void editService_ShouldReturnEditServiceInServiceRepository() {
        service.editService(acService);
        verify(mockServiceRepository, times(1)).editService(acService);
    }



}
