package com.fp.carservice;

import com.fp.carservice.services.PassayServiceImpl;
import com.fp.carservice.services.contracts.PassayService;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class PassayServiceTest {
    private PassayService passayService = new PassayServiceImpl();

    @Test
    public void When_PasswordGeneratedUsingPass_Then_Successful() {
        String password = passayService.generateRandomPassword();
        int specialCharCount = 0;
        for (char c : password.toCharArray()) {
            specialCharCount++;
        }
        assertTrue("Password validation failed in Pass", specialCharCount >= 2);
    }
}
