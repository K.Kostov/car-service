-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema carservice
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema carservice
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `carservice` DEFAULT CHARACTER SET latin1 ;
USE `carservice` ;

-- -----------------------------------------------------
-- Table `carservice`.`users`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `carservice`.`users` (
  `username` VARCHAR(65) CHARACTER SET 'latin1' NOT NULL,
  `password` VARCHAR(70) NOT NULL,
  `enabled` INT(11) NOT NULL DEFAULT '1',
  PRIMARY KEY (`username`),
  UNIQUE INDEX `id_UNIQUE` (`username` ASC)    )
ENGINE = InnoDB
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `carservice`.`authorities`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `carservice`.`authorities` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(65) CHARACTER SET 'latin1' NOT NULL,
  `authority` VARCHAR(50) NOT NULL DEFAULT 'ROLE_USER',
  PRIMARY KEY (`id`),
  INDEX `fk_users_authorities` (`username` ASC)    ,
  CONSTRAINT `fk_users_authorities`
    FOREIGN KEY (`username`)
    REFERENCES `carservice`.`users` (`username`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
AUTO_INCREMENT = 29
DEFAULT CHARACTER SET = latin1
ROW_FORMAT = COMPACT;


-- -----------------------------------------------------
-- Table `carservice`.`cars`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `carservice`.`cars` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `make` VARCHAR(45) NOT NULL,
  `model` VARCHAR(45) NOT NULL,
  `year` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC)    )
ENGINE = InnoDB
AUTO_INCREMENT = 7269
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `carservice`.`customers`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `carservice`.`customers` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `phone` VARCHAR(45) NOT NULL,
  `name` VARCHAR(65) NOT NULL,
  `email` VARCHAR(50) NOT NULL,
  `isDeleted` INT(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC)    ,
  UNIQUE INDEX `email` (`email` ASC)    )
ENGINE = InnoDB
AUTO_INCREMENT = 15
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `carservice`.`customer_cars`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `carservice`.`customer_cars` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `carId` INT(11) NOT NULL,
  `customerId` INT(11) NOT NULL,
  `VIN` VARCHAR(65) NOT NULL,
  `licensePlate` VARCHAR(45) CHARACTER SET 'latin1' NOT NULL,
  `isDeleted` INT(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC)    ,
  INDEX `fk_customercars_customer_idx` (`customerId` ASC)    ,
  INDEX `fk_customercars_cars_idx` (`carId` ASC)    ,
  CONSTRAINT `fk_customercars_cars`
    FOREIGN KEY (`carId`)
    REFERENCES `carservice`.`cars` (`id`),
  CONSTRAINT `fk_customercars_customers`
    FOREIGN KEY (`customerId`)
    REFERENCES `carservice`.`customers` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 19
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `carservice`.`services`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `carservice`.`services` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(65) NOT NULL,
  `price` DECIMAL(10,2) NOT NULL,
  `isDeleted` TINYINT(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC)    )
ENGINE = InnoDB
AUTO_INCREMENT = 56
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `carservice`.`visits`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `carservice`.`visits` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `date` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `customerCarId` INT(11) NOT NULL,
  `totalPrice` DECIMAL(10,2) NOT NULL DEFAULT '0.00',
  PRIMARY KEY (`id`),
  INDEX `fk_customercarvisit_customercars_idx` (`customerCarId` ASC)    ,
  CONSTRAINT `fk_customercarvisit_customercars`
    FOREIGN KEY (`customerCarId`)
    REFERENCES `carservice`.`customer_cars` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 110
DEFAULT CHARACTER SET = latin1;


-- -----------------------------------------------------
-- Table `carservice`.`visits_services`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `carservice`.`visits_services` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `visitsId` INT(11) NOT NULL,
  `serviceId` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `id_UNIQUE` (`id` ASC)    ,
  INDEX `fk_visits_services_services_idx` (`serviceId` ASC)    ,
  INDEX `fk_visits_services_visits_idx` (`visitsId` ASC)    ,
  CONSTRAINT `fk_visits_services_services`
    FOREIGN KEY (`serviceId`)
    REFERENCES `carservice`.`services` (`id`),
  CONSTRAINT `fk_visits_services_visits`
    FOREIGN KEY (`visitsId`)
    REFERENCES `carservice`.`visits` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 285
DEFAULT CHARACTER SET = latin1;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
